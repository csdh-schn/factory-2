+++
chapter = "Chapitre 4"
draft = true
title = "The Fourth Chapter"
weight = 4

+++
![Emma Goldman poster](/uploads/goldman_dance.jpeg "Emma Goldman poster")

To analyze the psychology of political violence is not only extremely difficult, but also very dangerous. If such acts are treated with understanding, one is immediately accused of eulogizing them. If, on the other hand, human sympathy is expressed with the _attentater_, one risks being considered a possible accomplice. Yet it is only intelligence and sympathy that can bring us closer to the source of human suffering, and teach us the ultimate way out of it.

> The primitive man, ignorant of natural forces, dreaded their approach, hiding from the perils they threatened. As man learned to understand Nature's phenomena, he realized that though these may destroy life and cause great loss, they also bring relief. To the earnest student it must be apparent that the accumulated forces in our social and economic life, culminating in a political act of violence, are similar to the terrors of the atmosphere, manifested in storm and lightning.

To thoroughly appreciate the truth of this view, one must feel intensely the indignity of our social wrongs; one's very being must throb with the pain, the sorrow, the despair millions of people are daily made to endure. Indeed, unless we have become a part of humanity, we cannot even faintly understand the just indignation that accumulates in a human soul, the burning, surging passion that makes the storm inevitable.

The ignorant mass looks upon the man who makes a violent protest against our social and economic iniquities as upon a wild beast, a cruel, heartless monster, whose joy it is to destroy life and bathe in blood; or at best, as upon an irresponsible lunatic. Yet nothing is further from the truth. As a matter of fact, those who have studied the character and personality of these men, or who have come in close contact with them, are agreed that it is their super-sensitiveness to the wrong and injustice surrounding them which compels them to pay the toll of our social crimes. The most noted writers and poets, discussing the psychology of political offenders, have paid them the highest tribute. Could anyone assume that these men had advised violence, or even approved of the acts? Certainly not. Theirs was the attitude of the social student, of the man who knows that beyond every violent act there is a vital cause.